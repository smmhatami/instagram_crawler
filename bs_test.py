from bs4 import BeautifulSoup

import requests
from selenium import webdriver
import os
from selenium.webdriver.common.keys import Keys
import time
import shutil
import json
import hashlib
import re

def login_to_instagram(driver, username, password):
    username_input = driver.find_element_by_name('username')
    pass_input = driver.find_element_by_name('password')
    username_input.send_keys(username)
    pass_input.send_keys(password)
    pass_input.send_keys(Keys.ENTER)

def get_and_save_image(img_src, saving_dest):
    r = requests.get(img_src, stream=True) 
    if r.status_code == 200:
        with open(saving_dest, 'wb') as f: 
            r.raw.decode_content = True
            shutil.copyfileobj(r.raw, f)

def get_and_save_video(video_src, saving_dest):
    r = requests.get(video_src, stream=True) 
    # download started 
    with open(saving_dest, 'wb') as f: 
        for chunk in r.iter_content(chunk_size = 1024*1024): 
            if chunk: 
                f.write(chunk)
    f.close()

def get_prof_pic_src(soup):
    profile_pic = soup.find("img", attrs={"data-testid" : "user-avatar"})
    profile_pic_source = profile_pic.attrs['src']
    return profile_pic_source

def parse_user_data(soup, username):
    # soup = BeautifulSoup()
    data = {'username' : username}
    # posts-followers-following
    info_section = soup.find('section', class_="zwlfE")
    posts_info = info_section.ul.find_all('li')
    data['posts'] = posts_info[0].span.span.text
    data['followers'] = posts_info[1].a.span.text
    data['followings'] = posts_info[2].a.span.text
    
    # name-website-bio
    user_info = info_section.find('div', class_="-vDIg")
    data['name'] = user_info.h1.text
    data['bio'] = user_info.span.text
    data['website'] = user_info.find('a', class_="yLUwa").text
    return data

def save_as_json(data, path):
    jsonString = json.dumps(data)
    jsonFile = open(path, "w", encoding="utf-8")
    jsonFile.write(jsonString)
    jsonFile.close()

def scroll_down(driver):
    driver.execute_script("window.scrollTo(0,document.body.scrollHeight)")
    
def add_new_posts(posts_links, new_posts):
    final_posts_links = []
    for link in posts_links:
        final_posts_links.append(link)
    for post in new_posts:
        link = post.a.attrs['href']
        if not link in final_posts_links:
            final_posts_links.append(link)
    return final_posts_links

def get_all_posts_links(driver, posts_count):
    posts_links = []
    while len(posts_links) < posts_count:
        page_soup = BeautifulSoup(driver.page_source, "html.parser")
        posts_pane = page_soup.find("article", class_="ySN3v").div.div
        new_posts = posts_pane.find_all("div", class_="v1Nh3 kIKUG _bz0w")
        posts_links = add_new_posts(posts_links, new_posts)
        scroll_down(driver)
        time.sleep(4)
    return posts_links

def get_post_medias(driver, post_id):
    medias = []
    sources = []
    media_id = 0
    while True:
        post_soup = BeautifulSoup(driver.page_source, "html.parser")
        medias_pane = post_soup.find("div", class_='_97aPb wKWK0')
        media_id += 1
        media_data = {'id' : media_id}
        media_re = re.compile("(img|video)")
        temp_medias = medias_pane.find_all(media_re)
        media = None
        for temp_media in temp_medias:
            if not temp_media.attrs['src'] in sources:
                media = temp_media
                break
        media_data['type'] = media.name
        media_data['src'] = media.attrs['src']
        sources.append(media_data['src'])
        medias.append(media_data)
        try:
            next_button = driver.find_element_by_class_name('_6CZji')
        except:
            break
        next_button.send_keys(Keys.ENTER)
    # save medias into file system
    for media in medias:
        time.sleep(2)
        if media['type'] == 'img':
            get_and_save_image(media['src'], f"{post_id}_{media['id']}.jpg")
        elif media['type'] == 'video':
            get_and_save_video(media['src'], f"{post_id}_{media['id']}.mp4")
    return medias

def get_and_save_posts(driver, posts_links):
    all_posts = []
    post_id = 0
    for post_link in posts_links:
        post_id += 1
        post_data = {'id' : post_id, 'link' : f"{instagram_url}{username}{post_link}"}
        # navigate to post's page
        driver.get(post_data['link'])
        time.sleep(delay_time)
        post_soup = BeautifulSoup(driver.page_source, "html.parser")
        # get post's description
        post_description = post_soup.find("div", class_='C4VMK')
        post_text = post_description.find('span', class_='').text
        post_data['text'] = post_text
        medias = get_post_medias(driver, post_id)
        post_data['medias_count'] = len(medias)
        post_data['medias'] = medias
        all_posts.append(post_data)
    return all_posts

# start selenium driver
cwd = os.getcwd().replace("\\", "/")
chromedriver_path = cwd + '/chromedriver'
driver = webdriver.Chrome(chromedriver_path)

# define urls and parameters
instagram_url = "https://www.instagram.com/"
login_url = instagram_url + "accounts/login/"
usernames = ['quera.ir']
testing_url = "https://codeforces.com/"
delay_time = 5

# go to login page and login
driver.get(login_url)
time.sleep(delay_time)
login_to_instagram(driver, 'smm.hatami', 'hatamiTakami')

# making data destination ready
if not os.path.exists("bs_data"):
    os.mkdir("bs_data")
os.chdir("bs_data")

for username in usernames:
    # fetch user's page
    time.sleep(delay_time)
    driver.get(f"{instagram_url}{username}/")
    my_soup = BeautifulSoup(driver.page_source, "html.parser")

    # making directory to save user's data
    username_hash = hashlib.sha1(username.encode("utf-8")).hexdigest()
    try:
        os.mkdir(f"{username_hash}")
    except OSError:
        print('file exists')
    os.chdir(f"{username_hash}")

    #saving user's data
    user_data = parse_user_data(my_soup, username)
    save_as_json(user_data, "user_data.json")
    get_and_save_image(get_prof_pic_src(my_soup), "prof_pic.jpg")

    posts_count = int(user_data['posts'])
    # posts_links = get_all_posts_links(driver, posts_count)
    posts_links = get_all_posts_links(driver, 9)
    # save posts medias and meta data
    all_posts_data = get_and_save_posts(driver, posts_links)
    save_as_json(all_posts_data, "posts.json")
    os.chdir("..")

